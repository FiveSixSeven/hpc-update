#! /usr/bin/python3

import os
import re
import keyring
import paramiko
import pickle
import socket
import collections
import xml.etree.ElementTree as ET

_TIMEOUT = 10.0

class User:
    """
    Main User Object
    """
    def __init__(self, configfile=None, user=None):
        if not user:
            self.user = os.getenv('USER')
        if configfile:
            self.configfile = configfile
        else:
            self.configfile = os.path.join(os.getenv('HOME'), '.hpcupdate') 
        self.servers = collections.OrderedDict()
            
    def add_server(self, resource_name, login_name, host_name, proxy=None):
        ''' Add a server to the user's dictionary '''
        self.servers.update({
            resource_name : _Server(resource_name, login_name, host_name, proxy, self.user)
        })

    def read_configfile(self):
        ''' Read the configure file according to _Server.unpack()'''
        with open(self.configfile, 'rb') as f:
            retlist = pickle.load(f)
        for element in retlist:
            self.servers.update({element[0] : _Server(*element)})

    def write_configfile(self):
        ''' Write the configure file according to _Server.unpack()'''
        retlist = []
        for server in self.servers.values():
            retlist.append(server.packup())
        pickle.dump(retlist, open(self.configfile, 'wb'))


class _ServerKeyring:
    """
    Individual Server Keyring Entry
    """
    def __init__(self, resource_name, user):
        self.resource_name = resource_name
        self.user = user

    def get_password(self):
        ''' Get Password from Keyring '''
        password = keyring.get_password(self.resource_name, self.user)
        if password:
            return password
        else:
            raise ServerKeyringError(self.resource_name, self.user, 'Password Not Found')

    def set_password(self, password):
        ''' Set Password on keyring '''
        keyring.set_password(self.resource_name, self.user, password)

class ServerKeyringError(Exception):
    def __init__(self, resource_name, user, message=''):
        super().__init__(message)


class _Server(object):
    """
    Individual Server Object
    """

    def __init__(self, resource_name, login_name, host_name, proxy, user):
        self.__resource_name = resource_name
        self.__login_name = login_name
        self.__host_name = host_name
        self.user = user
        self.proxy = proxy
        self.system = None

        #Particular Servers keyring
        self.keyring = _ServerKeyring(resource_name, user)

    @property
    def login_name(self):
        return self.__login_name

    @property
    def host_name(self):
        return self.__host_name

    @property
    def resource_name(self):
        return self.__resource_name

    def packup(self):
        '''Package all of the essential information to re-initialize an object'''
        return [self.__resource_name, self.login_name, self.host_name, self.proxy, self.user]

    def ssh_connect(self):
        """
        Set Up SSH connection
        """
        try:
            self.ssh_client = paramiko.SSHClient()
            self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh_client.connect(
                hostname=self.host_name, 
                username=self.login_name, 
                password=self.keyring.get_password(),
                timeout=_TIMEOUT
            )
        except paramiko.auth_handler.BadAuthenticationType:
            raise ServerAuthenError(self, 'Invalid Authentication')
        except socket.gaierror:
            raise NotReachableError(self, 'Server Not Reachable')

    def ssh_close(self):
        self.ssh_client.close()
        del self.ssh_client

    def __del__(self):
        if hasattr(self, 'ssh_client'):
            self.ssh_close()

    def ssh_cmd(self, cmd):
        ''' 
        Execute an SSH command over shh connection, assumes established connection

        Will add <?START_COMMAND?> and <?END_COMMAND?> tags to commands so that any 
        printed info will be ignored
        
        <?START_COMMAND?>
        ...
        Command Return
        ...
        <?END_COMMAND?>

        '''
        start = "<?START_SSH_COMMAND?>"
        end = "<?END_SSH_COMMAND?>"
        try:
            assert hasattr(self, 'ssh_client'), 'Must be connected to paramiko.SSHClient'
            cmd = "echo '{}'; {}; echo '{}'".format(start, cmd, end)
            stdin, stdout, stderr = self.ssh_client.exec_command(cmd)
        except Exception:
            raise ServerCommandError(self, cmd, 'command communication with server')

        #raw stdout and stderr
        raw_stdout = [line.strip() for line in stdout.readlines()]
        stderr = [line.strip() for line in stderr.readlines()]
        if stderr:
            raise ServerCommandError(self, cmd, 'command not found on server')
        
        #Trim the output of any extra data using a list slice
        start_index = raw_stdout.index(start) + 1
        end_index = raw_stdout.index(end)
        stdout = raw_stdout[start_index : end_index]

        return stdout, stderr

    def report(self, report_type, parse=True):
        ''' 
        Generate Report Information 
        
        report_type =  'summary' or 'detailed'

        Current Systems:
            PBS - OpenPBS, Torque
        '''
        possible_systems = {'PBS' : 'qstat -q'}
        systems_map = {'PBS' : (self._PBS_report, self._PBS_parse)}

        assert report_type == 'summary' or report_type == 'detailed', \
                "Report Type must be 'summary' or 'detailed'"

        #Open the ssh_client if it is not open
        temp_open = False
        if not hasattr(self, 'ssh_client'):
            temp_open = True
            self.ssh_connect()

        #Check Jobs Scheduler type
        if not self.system:
            for system, command in possible_systems.items():
                stdout, stderr = self.ssh_cmd(command)
                if not stderr:
                    self.system = system
                    break
            else:
                raise ServerError(self, 'unknown server scheduler type')

        output = systems_map[self.system][0](report_type)
        if parse:
            output = systems_map[self.system][1](report_type, output)

        #Close the ssh_client if it was opened in the beginning of this method
        if temp_open:
            self.ssh_close()
        return output

    def _PBS_report(self, report_type):

        summary_command = 'qstat -u $USER'
        detailed_command = 'qstat -fx'

        #Get the job summary, required in both summary and detailed cases
        stdout, stderr = self.ssh_cmd(summary_command)
        if report_type == 'summary':
            return stdout
        elif report_type == 'detailed':
            if not stdout:
                #In case there are no jobs, do not return []
                return []
            jobs = []
            temp = stdout[:]
            for line in temp:
                string = stdout.pop(0)
                if re.search('--------------', string.strip()):
                    break
            for line in stdout:
                jobs.append(line.strip().split()[0])

        #Get information for multiple jobs
        allcmd = detailed_command
        for job in jobs:
            allcmd += ' ' + job
        stdout, stderr = self.ssh_cmd(allcmd)
        
        return stdout

    def _PBS_parse(self, report_type, raw_output):
        retData = []

        _pbs_details_tags = (
            'Job_Id', 'Job_Name', 'Job_Owner', 'job_state', 'session_id', 'queue', 'server', \
            'Account_Name', 'ctime', 'mtime', 'etime', 'start_time', 'depend', 'submit_args',\
            'Output_Path', 'Error_Path', 'Mail_Points', 
        )

        if report_type == 'summary':
            for line in raw_output:
                line = line.strip('\n')
                if line:
                    retData.append(line)
            #Strip first line (ususally contains internal server name)
            return retData[1:]
        elif report_type == 'detailed':
            for job in raw_output:
                retData.append({})
                root = ET.fromstring(job)
                #Populate the list
                for child in root[0]:
                    if child.tag in _pbs_details_tags:
                        retData[-1].update({child.tag : child.text})
                #Populate empty values
                for tag in _pbs_details_tags:
                    if tag not in retData[-1]:
                        retData[-1].update({tag : '--'})
            return retData


class ServerError(Exception):
    def __init__(self, server, message=''):
        self.server = server
        super().__init__(message)

class ServerAuthenError(ServerError): pass

class NotReachableError(ServerError): pass

class ServerCommandError(ServerError):
    def __init__(self, server, cmd, message):
        self.cmd = cmd
        super().__init__(server, message)

class ServerTimeout(ServerCommandError): pass

