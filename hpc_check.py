#! /usr/bin/env python3
#=---------------------------------------------------------------------------=#
# HPC Update () 
#=---------------------------------------------------------------------------=#

import sys
import os
from collections import OrderedDict
import Resources

_CHECKERS = ('summary', 'detailed')
_CONFIG_FILE = os.path.join(os.getenv('HOME'), '.hpcupdate')

def main(ctype, config):
    """
    """
    if config:
        run_configuration(ctype)

    else:
        run_check(ctype)
        

def run_check(ctype):
    user = Resources.User(configfile=_CONFIG_FILE)
    try:
        user.read_configfile()
    except:
        raise ProgramErrors('Config File {} Not Found'.format(_CONFIG_FILE))
    servers = user.servers

    show_servers(servers)
    string = input("\nEnter server numbers to check (Enter for entire list):")

    check_servers = OrderedDict()
    if string:
        for num in string.split():
            try:
                keys = list(servers.keys())
                check_servers.update( { keys[int(num)-1] : servers[keys[int(num)-1]] })
            except TypeError:
                print("Invalid server number: {}".format(num))
                continue
    else:
        check_servers = servers

    print("\nChecking servers...")
    for key, value in check_servers.items():
        print("\n{}:".format(key))
        try: 
            lines = value.report(ctype)
            if lines:
                for line in lines:
                    print(line)
            else:
                print('No Jobs Found')
        except Exception as e:
            print('Error Reading servers: {}'.format(e))
        input('Press any key to continue')


def show_servers(servers):
    print("\nCurrent Servers:")
    for num, key in enumerate(servers):
        print("{:>3}) {:<10}".format(num+1, key))

def run_configuration(ctype):

    print("Running Configuration ...\n")
    pass

#    def update_server_list():
#        name = input('Enter symbolic servername:')
#        address = input('Enter user@servername:')
#        proxy = input('Enter proxy [blank if none]:')
#        login_name, host_name = address.split('@')
#
#        #No Duplicates of keys or values in servers
#        if name in servers.keys():
#            print('Symbolic Server already exists...')
#            return False
#        if address in servers.values():
#            print('Server address already exists...')
#            return False
#
#        #Update servers, if not 
#        try:
#            servers.update({ name : Resources.Server(address, proxy=proxy) })
#        except Resources.ServerError as ierr:
#            print(ierr)
#            return False
#        except Resources.KeyringError:
#            #General Keyring error
#            print("Server not found on keyring...")
#            try:
#                Resources.addKeyring(address)
#                servers.update({ name : Resources.Server(address, proxy=proxy) })
#            except keyring.backend.errors.PasswordSetError:
#                print('Error: Failed to add to keyring,' 
#                      'deleting {}: {} from servers'.format(name, address))
#            return False
#                
#        return True
#
#    
#    user = Resources.User()
#    servers
#    show_servers(servers)
#
#    #Prompt for the deletion of a preexisting server
#    if input('\nDelete current servers? (y/n):').lower() in ('y', 'yes'):
#        while True:
#            try:
#                num = int(input('\nEnter Server Number to DELETE (ctrl-D to finish):')) - 1 
#                servers.pop(list(servers.keys())[num])
#                show_servers(servers)
#            except (ValueError, KeyError) as ierr:
#                print(ierr)
#                continue
#            except EOFError:
#                print('\n..Done')
#                break
#
#    #Prompt for the change of a preexisting server
#    show_servers(servers)
#    if input('\nChange current servers? (y/n):').lower() in ('y', 'yes'):
#        while True:
#     
#     try:
#                num = int(input('\nEnter Server Number to REPLACE (ctrl-D to finish):')) - 1 
#                if update_server_list():
#                    servers.pop(list(servers.keys())[num])
#                show_servers(servers)
#            except (ValueError, KeyError) as ierr:
#                print(ierr)
#                continue
#            except EOFError:
#                print('\n..Done')
#                break
#    
#    #Prompt for the addition of a new server
#    show_servers(servers)
#    if input('\nAdd new servers? (y/n):').lower() in ('y', 'yes'):
#        while True:
#            #Server Entry procedure
#            try:
#                print('\nNew server (ctrl-D to finish)...')
#                if not update_server_list():
#                    continue
#                show_servers(servers)
#            except EOFError:
#                print('\n..Done')
#                break
#
#    print("\nwriting file {}...".format(_PICKLE_PATH))
#    pickle.dump(servers, open(_PICKLE_PATH, 'wb'))



def parse_argv(argv):
    import argparse

    welcome = 'HPC Jobs Checker'
    parser = argparse.ArgumentParser( 
        #usage='%(prog)s [options] positional',
        description=welcome, 
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        '--config',
        action='store_true',
        default=False,
        help='Run configuration setup [%(default)s]'
    )
    parser.add_argument(
        'ctype',
        action='store',
        type=str,
        nargs='?',
        help='HPC check type'
    )
    print()
    args  = parser.parse_args()

    #TODO remove hard code of the checkers
    if not args.config and args.ctype not in _CHECKERS:
        print('ctype : support checker types ('.format(args.ctype), ', '.join(_CHECKERS), ')')
        sys.exit(1)

    return args

class ProgramErrors(Exception):
    def __init__(self, message):
        super().__init__(message)
    

if __name__ == '__main__':
    args = parse_argv(sys.argv[1:])
    main(args.ctype, args.config)
