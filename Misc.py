#! /usr/bin/env python

def ask_user(prompt):
    """
    Ask the user a yes or no question, 
    return True/False
    """
    if input(prompt + ' [y/Y] :').lower() in ('y', 'yes'):
        return True
    else:
        return False
